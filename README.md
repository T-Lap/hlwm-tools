# hlwm-tools

Tools for herbstluftwm.

## HLWM color changer ( hlwincolor )

### Dependencies 
* Zenity

### Install
Clone the repository `git clone https://gitlab.com/T-Lap/hlwm-tools.git` then move into the hlwincolor folder `cd hlwm-tools/hlwincolor/`. Then copy the script to your $PATH `sudo cp src/change-color /usr/bin/` and to the herbstluftwm directory `cp src/maincolor $HOME/.config/herbstluftwm/`. If you like to have a program starter in your application menu `sudo cp src/change-color.desktop /usr/share/applications/`.

Now you need to do some changes to your `$HOME/.config/herbstluftwm/autostart` file.

1. add `source $HOME/.config/herbstluftwm/maincolor` at the top after `#!/usr/bin/bash`.
2. Change the color settings where you like to have the change. I did:
* `hc set frame_border_active_color "$MAINCOLOR"`
* `hc attr theme.active.color "$MAINCOLOR"`
* `hc attr theme.active.inner_color "$MAINCOLOR"`
* `hc attr theme.active.outer_color "$MAINCOLOR"`

Or just use the `./install.sh` script, it make the changes to your autostart file and copy the scripts.




