#!/bin/bash
# Install script for Maincolor changer at HerbstluftWM.
# Please make the install script executeble. " $ chmod +x install.sh "

sed -i '4 i source $HOME/.config/herbstluftwm/maincolor' $HOME/.config/herbstluftwm/autostart
sed -i -E 's/hc set frame_border_active_color.*/hc set frame_border_active_color "$MAINCOLOR"/' $HOME/.config/herbstluftwm/autostart
sed -i -E 's/hc attr theme.active.color.*/hc attr theme.active.color "$MAINCOLOR"/' $HOME/.config/herbstluftwm/autostart
sed -i -E 's/hc attr theme.active.inner_color.*/hc attr theme.active.inner_color "$MAINCOLOR"/' $HOME/.config/herbstluftwm/autostart
sed -i -E 's/hc attr theme.active.outer_color.*/hc attr theme.active.outer_color "$MAINCOLOR"/' $HOME/.config/herbstluftwm/autostart
sed -i -E 's/hc set frame_bg_active_color.*/hc set frame_gb_active_color "$MAINCOLOR"/' $HOME/.config/herbstluftwm/autostart
cp src/maincolor $HOME/.config/herbstluftwm/
sudo cp src/change-color.desktop /usr/share/applications/
sudo cp src/change-color /usr/bin/

# herbstclient reload
